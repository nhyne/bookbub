require 'json'
require 'csv'
require 'trie'
class GenreSuggestor

  def initialize(books_file, keywords_file)
    books_read_file = File.read(books_file)
    @trie = Trie.new
    @books_hash = JSON.parse(books_read_file).sort_by { |b| b['title'] }
    @keywords_hash = {}
    first = true
    CSV.foreach(keywords_file) do |row|
      unless first
        word = row[1].strip
        if @keywords_hash[word]
          @keywords_hash[word][:genres][row[0]] = row[2].to_i 
        else
          @keywords_hash[word] = { genres: { row[0] => row[2].to_i } }
        end
        @trie.add(word)
      end
      first = false
    end
  end

  def calculate_books
    @books_hash.each do |book|
      puts book['title']
      best_genres(genre_avg_count(word_occurances(book['description']))).reverse.each do |genre|
        puts "genre: #{genre[:name]}, score: #{genre[:points]}"
      end
      puts "\n"
    end
  end

  def genres_for_keyword(keyword)
    @keywords_hash[keyword][:genres].keys
  end

  def keyword_genre_score(keyword, genre)
    @keywords_hash[keyword][:genres][genre]
  end

  def genre_avg_count(word_count)
    genre_scores = {}
    word_count.each_key do |word|
      genres_for_keyword(word).each do |genre|
        if genre_scores[genre]
          genre_scores[genre] = { 
            avg: (genre_scores[genre][:avg] * genre_scores[genre][:count] + keyword_genre_score(word, genre) * word_count[word]) / (genre_scores[genre][:count] + word_count[word]),
            count: genre_scores[genre][:count] + word_count[word]
          }
        else
          genre_scores[genre] = { avg: keyword_genre_score(word, genre), count: word_count[word] }
        end
      end
    end
    return genre_scores
  end

  # given a hash of genres and 
  def best_genres(score_hash)
    returner = []
    score_hash.each_key do |genre|
      points = score_hash[genre][:avg] * score_hash[genre][:count]
      returner << { 'name': genre, points: points}
    end
    sorted = returner.sort_by { |k| k[:points] }
    sorted.last(3)
  end

  def word_occurances(description)
    returner = {}
    node = @trie.root
    description.each_char do |char|
      if node.walk!(char)
        if node.terminal?
          returner[node.full_state] = returner[node.full_state].nil? ? 1 : returner[node.full_state] + 1
        end
      else
        node = @trie.root
      end
    end
    return returner
  end
end

# notes for things I would like to have done:

# I dont think this needs to actually be its own class, it really just builds ontop of the trie class
  # and keeps track of the keywords

# The way that I only return 3 

geners_getter = GenreSuggestor.new('sample_books.json', 'sample_genre_keyword_value.csv')
geners_getter.calculate_books