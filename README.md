# Steps to Run

1. bundle install
2. ruby score_calculator.rb

* I ran this using ruby 2.5.0 and 2.4.2, however my gems are installed globally on both machines.

# Trade offs and edge cases

The biggest edge case I ran into was when a keyword could appear inside of another keyword. For instance "fight" and "fighting" could both easily be under the action genre with different keyword scores. Initially I was planning on running through the entire description for each word to handle that, but the runtime would have been terrible. Instead I decided to use a trie to handle that edge case. The trie allowed me to check an see if a word was a leaf of the tree as well as to continue walking through it if there were other children.

I definitely made some tradeoffs. Since I'm using a hash and a trie, the program is memory heavy, but will run pretty quickly. 

I also put it in a class, which wasn't necessary. I did it more for my own sanity and readability than anything else. 

It also probably isn't necessary to store the csv or the json file in memory. I'm sure that if I spent more time on it I could find a way to read from the file system for the values but we would lose a lot of speed.

# Approximate time

I think I spent a little over 3 hours, probably 3.5. I got an initial solution with nested for loops done in around an hour and took a break to think on a better solution before sitting back down and working on it.